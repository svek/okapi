# Project: ML_CCZ4_gaugewave
# Purpose: Run a Gaugewave with CCZ4, Cactus
# Initially Written By: Luke at 2017-04-20


ActiveThorns = "CoordBase SymBase Boundary NanChecker CartGrid3d Time MoL CarpetIOBasic CarpetIOScalar IOUtil Carpet CarpetLib CarpetReduce CarpetInterp CarpetSlab CarpetIOASCII ADMBase StaticConformal SpaceMask Slab Periodic Exact GenericFD CoordGauge ADMCoupling LoopControl ML_CCZ4 ML_CCZ4_Helper GenericFD TMuNuBase SphericalSurface ADMMacros TimerReport GaugeWave HDF5 CarpetIOHDF5"

ADMBase::evolution_method         = "ML_CCZ4"
ADMBase::lapse_evolution_method   = "ML_CCZ4"
ADMBase::shift_evolution_method   = "ML_CCZ4"
ADMBase::dtlapse_evolution_method = "ML_CCZ4"
ADMBase::dtshift_evolution_method = "ML_CCZ4"

# We use the BBH gauge to ensure it is regression-tested.
ML_CCZ4::harmonicN           = 2.0     # harmonic slicing
ML_CCZ4::harmonicF           = 1.0     # harmonic sllicing
ML_CCZ4::ShiftGammaCoeff     = 0.0
ML_CCZ4::BetaDriver          = 0.0 #killing the shift
ML_CCZ4::advectLapse         = 0.0
ML_CCZ4::advectShift         = 0.0 
ML_CCZ4::GammaShift          = 0.0
ML_CCZ4::dampk1              = 0.0
ML_CCZ4::dampk2              = 0.0

ML_CCZ4::MinimumLapse        = 1.0e-8
ML_CCZ4::conformalMethod     = 1 # 1 for W, 0 for phi
ML_CCZ4::rhs_boundary_condition     = "scalar"   # was "static"

ML_CCZ4::apply_dissipation   = "always"
ML_CCZ4::EpsDiss             = 0.1

Boundary::radpower                     = 2

#############################################################
# Grid
#############################################################

CoordBase::domainsize                   = minmax

CoordBase::boundary_size_x_lower        = 3
CoordBase::boundary_size_y_lower        = 3
CoordBase::boundary_size_z_lower        = 3
CoordBase::boundary_shiftout_x_lower    = 1
CoordBase::boundary_shiftout_y_lower    = 1
CoordBase::boundary_shiftout_z_lower    = 1

CoordBase::boundary_size_x_upper        = 3
CoordBase::boundary_size_y_upper        = 3
CoordBase::boundary_size_z_upper        = 3
CoordBase::boundary_shiftout_x_upper    = 0
CoordBase::boundary_shiftout_y_upper    = 0
CoordBase::boundary_shiftout_z_upper    = 0

CartGrid3D::type                        = "coordbase"
CartGrid3D::domain                      = "full"
CartGrid3D::avoid_origin                = "no"

Periodic::periodic = "yes"

CoordBase::xmin                         = 0
CoordBase::ymin                         = 0
CoordBase::zmin                         = 0

CoordBase::xmax                         = 1
CoordBase::ymax                         = .1
CoordBase::zmax                         = .1

CoordBase::dx                           = 0.0025
CoordBase::dy                           = 0.01
CoordBase::dz                           = 0.01

#############################################################
# Carpet
#############################################################

Carpet::ghost_size                      = 3
Carpet::domain_from_coordbase           = "yes"
Carpet::init_3_timelevels               = "no"
Carpet::poison_new_timelevels           = yes

#############################################################
# Timers
#############################################################

# Do not commit these as testsuite output as they will change on
# different machines.  They are useful for performance monitoring in
# automated build and test systems.
TimerReport::output_all_timers_readable = yes
TimerReport::n_top_timers               = 40
TimerReport::output_schedule_timers     = no

#############################################################
# Time integration
#############################################################

Cactus::terminate                         = "time"
Cactus::cctk_final_time		= 10
# Use 0.5 for RK4 and 0.25 for ICN
Time::dtfac                           = 0.5

MethodOfLines::ode_method             = "RK4"
MethodOfLines::MoL_Intermediate_Steps = 4

#MethodOfLines::ode_method             = "generic"
#MethodOfLines::generic_type             = "RK"
#MethodOfLines::MoL_Intermediate_Steps = 1

MethodOfLines::MoL_Num_Scratch_Levels = 1
MethodOfLines::MoL_NaN_Check          = "no"
MethodOfLines::initial_data_is_crap   = "no"

#############################################################
# Initial data
#############################################################

#ADMBase::initial_data = "exact"
#ADMBase::initial_lapse = "exact"
#ADMBase::initial_shift = "exact"
#ADMBase::initial_dtlapse = "exact"
#ADMBase::initial_dtshift = "exact"
#
#Exact::exact_model = "Minkowski/shifted gauge wave"
#Exact::Minkowski_gauge_wave__what_fn = "sin"
#Exact::Minkowski_gauge_wave__amplitude = 0.1
#Exact::Minkowski_gauge_wave__omega = 1
#Exact::Minkowski_gauge_wave__lambda = 0.577350269189626
#
#Exact::rotation_euler_psi = -1.9216757376671543544
#Exact::rotation_euler_theta = 0.66214523564555227398
#Exact::rotation_euler_phi = 1.2199169159226388841
#
#Exact::exact_order = 4
#Exact::exact_eps = 1e-2 # This is large to ensure regression tests are insensitive to roundoff

#############################################################
# Initial data
#############################################################

ADMBase::initial_data                = "GaugeWave"
ADMBase::initial_lapse               = "GaugeWave"
ADMBase::initial_shift               = "GaugeWave"
ADMBase::initial_dtlapse             = "GaugeWave"
ADMBase::initial_dtshift             = "GaugeWave"

GaugeWave::amp = 0.8
GaugeWave::period = 1

GaugeWave::psi = 0.0
GaugeWave::theta = 0.0
GaugeWave::phi = 0.0

#############################################################
# Output
#############################################################

ActiveThorns = "Volomnia"
Volomnia::symm_weight = 4

IO::out_dir                   = "data/"
IO::out_fileinfo              = "none"
IO::parfile_write             = "no"
CarpetIOBasic::outInfo_every        = 1
CarpetIOBasic::outInfo_vars         = "
        Carpet::physical_time_per_hour
        ADMBase::alp
"

IOASCII::out0D_criterion = "divisor"
IOASCII::out0D_every = 1
IOASCII::out0D_dir = "data/timing/"
IOASCII::out0D_vars = "
	Carpet::physical_time_per_hour
	Volomnia::grid_volume
	Volomnia::cell_volume
	Carpet::timing
"

#IOASCII::out1D_every          = 8
IOASCII::out1D_every          = 1
IOASCII::out_precision        = 19
IOASCII::out1D_x              = "yes"
IOASCII::out1D_y              = "no"
IOASCII::out1D_z              = "no"
IOASCII::out1D_d              = "no"
IOASCII::out1D_vars           = "
	ADMBase::gxx
	ADMBase::kxx
	ML_CCZ4::ML_mom
	ML_CCZ4::ML_Ham
	ML_CCZ4::ML_cons_detg
	ML_CCZ4::ML_cons_Gamma
	ML_CCZ4::ML_cons_traceA
	ML_CCZ4::ML_curv
	ML_CCZ4::ML_dtlapse
	ML_CCZ4::ML_dtshift
	ML_CCZ4::ML_Gamma
	ML_CCZ4::ML_lapse
	ML_CCZ4::ML_log_confac
	ML_CCZ4::ML_metric
	ML_CCZ4::ML_shift
	ML_CCZ4::ML_trace_curv
	ML_CCZ4::ML_Theta
"
#IOASCII::out_precision        = 19
#IOASCII::out1D_x              = "yes"
#IOASCII::out1D_y              = "no"
#IOASCII::out1D_z              = "no"
#IOASCII::out1D_d              = "no"
#IOASCII::out1D_vars           = "ADMBase::gxx ADMBase::kxx ML_CCZ4::ML_mom ML_CCZ4::ML_Ham ML_CCZ4::ML_cons_detg ML_CCZ4::ML_cons_Gamma ML_CCZ4::ML_cons_traceA ML_CCZ4::ML_curv ML_CCZ4::ML_dtlapse ML_CCZ4::ML_dtshift ML_CCZ4::ML_Gamma ML_CCZ4::ML_lapse ML_CCZ4::ML_log_confac ML_CCZ4::ML_metric ML_CCZ4::ML_shift ML_CCZ4::ML_trace_curv ML_CCZ4::ML_Theta"

IOHDF5::out1D_vars = "ADMBase::gxx ADMBase::kxx ML_CCZ4::ML_mom ML_CCZ4::ML_Ham ML_CCZ4::ML_cons_detg ML_CCZ4::ML_cons_Gamma ML_CCZ4::ML_cons_traceA ML_CCZ4::ML_curv ML_CCZ4::ML_dtlapse ML_CCZ4::ML_dtshift ML_CCZ4::ML_Gamma ML_CCZ4::ML_lapse ML_CCZ4::ML_log_confac ML_CCZ4::ML_metric ML_CCZ4::ML_shift ML_CCZ4::ML_trace_curv ML_CCZ4::ML_Theta"
IOHDF5::out1D_criterion                 = "divisor"
IOHDF5::out1D_d                         = "no"
IOHDF5::out1D_every                     = 1
IOHDF5::one_file_per_group = Yes

IOHDF5::out2D_vars = "ADMBase::gxx ADMBase::kxx ML_CCZ4::ML_mom ML_CCZ4::ML_Ham ML_CCZ4::ML_cons_detg ML_CCZ4::ML_cons_Gamma ML_CCZ4::ML_cons_traceA ML_CCZ4::ML_curv ML_CCZ4::ML_dtlapse ML_CCZ4::ML_dtshift ML_CCZ4::ML_Gamma ML_CCZ4::ML_lapse ML_CCZ4::ML_log_confac ML_CCZ4::ML_metric ML_CCZ4::ML_shift ML_CCZ4::ML_trace_curv ML_CCZ4::ML_Theta"
IOHDF5::out2D_criterion                 = "divisor"
IOHDF5::out2D_every                     = 1


