# The Runge Kutta Finite Differencing Toy Code

This small standalone toy code was written by Michael Dumbser. It solves the CCZ4
system with pure FD and a special dissipation operator. It was written to demonstrate
that such a scheme can solve CCZ4 equations stably. As initial data, only the analytical
GaugeWave is integrated.

For more information, see the README.md file within the Okapi root directory.

## How to compile

Just compile all files with gfortran together.