# Okapi: Cactus thorn for FO-CCZ4

Provide a minimal thorn to implement the **First Order CCZ4 system** for
solving Einsteins Equations.

This code was initially written in 2018 and borrows the infrastructural ideas
from the [Antelope thorn](https://bitbucket.org/emost/antelope), a modern
spacetime code for the Einstein Toolkit which uses the C++ TensorTemplates.
In contrast, Okapi is a Fortran thorn which uses C/C++ only for administrative
tasks (such as diverse Cactus registrations).

* Author: Sven Koeppel <koeppel@fias.uni-frankfurt.de>
* Maintainer(s): Sven Koeppel <koeppel@fias.uni-frankfurt.de>
* License: GPLv3

## The PDE and numerical scheme

The First Order CCZ4 PDE system was published in https://arxiv.org/abs/1707.09910.
This paper is written in the context of the ExaHyPE code, the PDE is solved
there with Dumbsers sophisticated ADER-DG scheme which has a Riemann solver at its
heart, next to a limiting high order Finite Volume scheme which solves the PDE
around spacetime singulartiies.

In contrast, in this Cactus thorn we solve the same PDE with basic
Finite Differencing methods, without a Riemann Solver but with artificial
dissipation. We use the Cactus infrastructure, such as the MoL thorn for time
evolution.

This is a *Fortran* thorn by purpose: The PDE development and reference
implementation has happened in Fortran and is actually still ongoing.
Sticking to Fortran allows us to easily sync new features from the
[ExaHyPE](http://www.exahype.eu) code. Furthermore, Fortran excels at
readability and performance while having a decent code base which does
not require the latest C++ features. Therefore, the Okapi is probably
not even the slowest animal around. However, the code has not been tuned
for speed at all so far. For instance, for the time being it does not
exploit the sparsity in the system matrix (while this is easy to
implement, thought).

## Other codes which also solve FO-CCZ4: Antelope and RKFDToy

We also implemented the FO-CCZ4 system in the TensorTemplates formuliation (i.e. without
any Fortran) within the Antelope thorn. Therefore, it can do the same nowadays. The
PDE parameters may sound a bit different, thought. Anyway the Antelope thorn and the
Okapi thorn are quite similar. We chose to seperate them to keep any Fortran out of
Antelope. You can think of Okapi as Dumbsers code ported to Cactus, using Antelope as
a basis.

Furthermore, there is the standalone "toy" code `RKFD` (or `FDRK`) which is a small
Runge-Kutta finite differencing demonstrator code. It is stored in this thorn for
illustrative purposes. It is a pure Fortran code which also acts as a kind of basis
for this thorn. You can think of Okapi as a port of RKFD to Cactus.

## How to start doing Physics with this thorn

Inspect the parameter files available in the `par` directory. They should run out of
the box and solve some demonstrative physical problems.

## Status of this thorn

Development started 2018-01-26, so far most code has been written and it
compiles. Compared to Antelope, we lack however a good understanding of
the interaction with the Cactus infrastructure regarding

  * Reflection !
  * Ghostlayers (?)

Once this is finally fixed, we will remove this comment.

## Fortran compilation notes

### Fortran real vs C doubles

We used to compile our Fortran code in ExaHyPE with the GCC options
`-fdefault-real-8 -fdefault-double-8` (gcc) or `-r8` (intel).

However, doing so in Cactus OPTIONLISTS breaks several Fortran thorns in
Cactus. Therefore, we replaced all `REAL` data types in Fortran with
`CCTK_REAL` instead. This ensures interoperability between C and Fortran
doubles.

### Line lengths

By default, Fortran has a fixed line length maximum. However, in this code
we have a large number of very long lines (>132 characters per line).

Typical intel compiler flags we use are `-c -O2 -r8 -align -fpp` and then we
have no problem with line length.
With GCC, make sure you have `-ffree-line-length-none`, we experienced that
`-ffixed-line-length-none` is not sufficient.

### The preprocessor

Make sure you run the `-cpp` C preprocessor on Fortran files. In Cactus,
`*.F90` files are preprocessed but `*.f90` files are not.

## Various Cactus build tips

On Ubuntu/GCC, this code built well with the `itpgu.cfg` configuration,
available at the https://bitbucket.org/relastro/simfactory2

In order to build a single thorn, during development to quickly see the errors:

```
make yourconfigname-build options=$PWD/simfactory/mdb/optionlists/itpgu.cfg THORNLIST=thornlists/yourthornlist.th  PROMPT=no BUILDLIST=Okapi 
```

In order to build everything:

```
make -j5 yourconfigname options=$PWD/simfactory/mdb/optionlists/itpgu.cfg THORNLIST=thornlists/yourthornlist.th PROMPT=no BUILDLIST=Okapi 
```


