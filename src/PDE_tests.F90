! Toy problem PDEs

RECURSIVE SUBROUTINE PDEFusedSrcNCP_Toy(Src_BgradQ,Q,gradQ)
    USE typesDef, ONLY : nVar,  nDim, EQN, d
    IMPLICIT NONE
    ! Argument list 
    REAL, INTENT(IN)  :: Q(nVar), gradQ(nVar,d)  
    REAL, INTENT(OUT) :: Src_BgradQ(nVar)
    ! Local variables
    REAL :: Qx(nVar), Qy(nVar), Qz(nVar)
    !

    Qx = gradQ(:,1) 
    Qy = gradQ(:,2)
    Qz = gradQ(:,3)

    !! This is the acoustic PDE which Dumbser uses with Gauss pulse ID
    !!
    !Src_BgradQ(1) = Qx(4) 
    !Src_BgradQ(2) = Qy(4) 
    !Src_BgradQ(3) = Qz(4) 
    !Src_BgradQ(4) = Qx(1) + Qy(2) + Qz(3)
    
    ! An advection equation for all fields, advecting any ID in z direction
    Src_BgradQ = - Qz
    !
END SUBROUTINE PDEFusedSrcNCP_Toy

RECURSIVE SUBROUTINE PDEFusedSrcNCP_Qt(Src_BgradQ,Q,gradQ)
    USE typesDef, ONLY : nVar,  nDim, EQN, d
    IMPLICIT NONE
    ! Argument list 
    REAL, INTENT(IN)  :: Q(nVar), gradQ(nVar,d)  
    REAL, INTENT(OUT) :: Src_BgradQ(nVar)
    ! Local variables
    !
    ! The PDE \partial_t Q = 0
    Src_BgradQ = 0
    !
END SUBROUTINE PDEFusedSrcNCP_Qt
