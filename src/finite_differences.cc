#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"

#include "finite_differences.h"

/* The only reason why this file is C++ and not C is the use of constexpr */

#include <fenv.h> // nan tracker
#include <math.h> // std::isnan

/*
 * For literature about the Finite Differencing coefficients and the involved
 * ghostzones, see for instance
 * 
 *   https://en.wikipedia.org/wiki/Finite_difference_coefficient
 *   https://www.researchgate.net/publication/242922236_Generation_of_Finite_Difference_Formulas_on_Arbitrarily_Spaced_Grids
 *   -> available at http://www.ams.org/journals/mcom/1988-51-184/S0025-5718-1988-0935077-0/S0025-5718-1988-0935077-0.pdf
 *
 */

/* Handy macro for computing offsets in a certain direction */
#define OFFSET(cctkGH,dir,i,j,k,pos)  CCTK_GFINDEX3D(cctkGH,i+(dir==0 ? pos : 0),j+(dir==1 ? pos : 0),k+(dir==2 ? pos : 0))

/**
 * Macro for computing central finite differences.
 * Having this as macro instead of C++ template makes it just easier to read
 * and implement. Since all macro arguments are compile time known, all loops
 * are unrolled.
 * 
 * If you have trouble with the Pragma, remove it.
 * This was it, removed:
 * 	_Pragma("omp parallel for simd")\
 **/
#define CFD(dQ,var,field,order,stencil,dx,i,j,k) \
	for(int dir=0; dir<3; dir++) {\
		dQ[dir][var] = 0;\
		for(int ind=0; ind<order; ind++) {\
			int pos = ind - order/2;\
			dQ[dir][var] += stencil[ind]/dx[dir]*field[OFFSET(cctkGH,dir,i,j,k,pos)];\
		}\
	}

/**
 * Macro for computing one-sided (advective) finite differences.
 *
 **/
#define UPWIND_FD(dQ,beta,var,field,order,stencil,dx,ijk,i,j,k) \
	for(int dir=0; dir<3; dir++) {\
		dQ[dir][var] = 0;\
		for(int ind=0; ind<order; ind++) {\
			dQ[dir][var] += \
				  (beta[dir][ijk] > 0 ? +1 : -1) \
				* stencil[ind] / dx[dir] \
				* field[OFFSET(cctkGH,dir,i,j,k,ind)];\
		}\
	}
	
/**
 * The FO-CCZ4 system decouples into an ODE and PDE part. Use NANDIFF to
 * set the ODE part safely to "NAN". Once this works, we should not set
 * the ODE part at all.
 **/
// this is c++:
//#include <limits> // std::numeric_limits
// std::numeric_limits<double>::signaling_NaN();
// but this is pure c:
#define NANFD(dQ,var,field,order,stencil,dx,i,j,k) \
	for(int dir=0; dir<3; dir++) {\
		 dQ[dir][var] = NAN; \
	}
/**
 * Or use this to do nothing.
 * 
 * From my mini benchmarks, it does not make a difference anyway.
 **/
#define NO_FD(dQ,var,field,order,stencil,dx,i,j,k)

/**
 * This is a macro to make sure stencils are inlined.
 **/
#define CENTRAL_DIFF(dQ, order, stencil, dx, i,j,k) \
	for(int n=0; n<nVar; n++) {\
		if(state[n]) {   CFD(dQ, n, state[n], order, stencil, dx, i, j, k); }\
		else	     { NO_FD(dQ, n, state[n], order, stencil, dx, i, j, k); }\
	}
	
#define UPWINDING_DIFF(dQ, beta, order, stencil, dx, ijk, i,j,k) \
	for(int n=0; n<nVar; n++) {\
		if(state[n]) { UPWIND_FD(dQ, beta, n, state[n], order, stencil, dx, ijk, i, j, k); }\
		else	     {     NO_FD(dQ, n, state[n], order, stencil, dx, i, j, k); }\
	}

/*
int Okapi_determine_ghostzone_size(const cGH *cctkGH) {
	DECLARE_CCTK_PARAMETERS;
	DECLARE_CCTK_ARGUMENTS;
	
	// order is a cactus integer parameter.
	if(order == -1) {
		int determined_order = cctk_nghostzones[0];
		
		// check if all ghostzones have same size.
		for(int d = 1; d < 3; d++) {
			if(determined_order != cctk_nghostzones[d]) {
				determined_order = -1; break;
			}
		}
		
		if(determined_order == -1) {
			CCTK_ERROR("Cannot determine FD order from ghostzone size. Specify the parameter 'order' explicitely.");
		} else {
			return determined_order;
		}
	} else {
		return order;
	}
}
*/
	
void Okapi_central_finite_differences(const cGH *cctkGH, int i, int j, int k, double gradQ[nDim][nVar]) {
	DECLARE_CCTK_PARAMETERS; // defines int order
	DECLARE_CCTK_ARGUMENTS;  // defines the fields
	
	// The central finite difference stencils for order p requires p/2 ghost zones.
	// These arrays read like coefficients for positions { -n, -n+1, ..., -1, 0, +1, ... n-1, n }
	constexpr double stencil2[] = {-1./2., 0, 1./2. };
	constexpr double stencil4[] = {1./12., -2./3., 0, 2./3. ,-1./12.};
	constexpr double stencil6[] = {-1/60., 3./20., -3./4., 0, 3./4., -3./20., 1./60.};
	
	const double delta[] = { CCTK_DELTA_SPACE(0), CCTK_DELTA_SPACE(1), CCTK_DELTA_SPACE(2) };
	
	ALIAS_STATE(state); // could also use ALIAS_PDE_PART

	switch(order) {
		case 2: CENTRAL_DIFF(gradQ, 2, stencil2, delta, i,j,k); break;
		case 4: CENTRAL_DIFF(gradQ, 4, stencil4, delta, i,j,k); break;
		case 6: CENTRAL_DIFF(gradQ, 6, stencil6, delta, i,j,k); break;
		default: 
			CCTK_WARN(CCTK_WARN_ABORT, "Requested central finite differencing order is not implemented.");
	}
}

void Okapi_advective_finite_differences(const cGH *cctkGH, const CCTK_REAL* const beta[nDim], int i, int j, int k, double Upwind_gradQ[nDim][nVar]) {
	DECLARE_CCTK_PARAMETERS; // defines int order
	DECLARE_CCTK_ARGUMENTS;  // defines the fields
	
	// The one-sided finite differencing stencils at order p require p ghost cells.
	// These arrays read like coefficients for positions { 0, 1, ... n }
	constexpr double stencil1[] = { -1, 1 };
	constexpr double stencil2[] = { -3./2., 2., -1./2. };
	constexpr double stencil3[] = { -11./6., 3., -3./2., 1./3. };
	
	const double delta[] = { CCTK_DELTA_SPACE(0), CCTK_DELTA_SPACE(1), CCTK_DELTA_SPACE(2) };
	
	ALIAS_PDE_PART(state);
	const int ijk = CCTK_GFINDEX3D(cctkGH,i,j,k);
	
	// we understand the requested order as Central FD order.
	const int one_sided_order = order / 2;
	
	switch(one_sided_order) {
		case 1: UPWINDING_DIFF(Upwind_gradQ, beta, 1, stencil1, delta, ijk, i, j, k); break;
		case 2: UPWINDING_DIFF(Upwind_gradQ, beta, 2, stencil2, delta, ijk, i, j, k); break;
		case 3: UPWINDING_DIFF(Upwind_gradQ, beta, 3, stencil3, delta, ijk, i, j, k); break;
		default:
			CCTK_VInfo(CCTK_THORNSTRING, "Requested advective finite differencing order %d is not implemented (derived from central differencing order %d)", one_sided_order, order);
			CCTK_ERROR("Requested advective finite differencing order is not implemented.");
	}
}
