#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "Symmetry.h"
#include <stdio.h> // sprintf

#include "state_vector.h"

extern "C" void Einstein_RegisterSlicing(const char* const);

int Okapi_CoordRegister(void)
{
  DECLARE_CCTK_PARAMETERS;

  CCTK_INFO("Registering slicings.");

  Einstein_RegisterSlicing("Okapi");

  return 0;
}


void Okapi_MoLRegister(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  int ierr=0;

  CCTK_INFO("Registering evolution variables with MoL.");
  
  const char *state_vector_names[] = { "g11", "g12", "g13", "g22", "g23", "g33", "A11", "A12", "A13", "A22", "A23", "A33", "Theta", "G1", "G2", "G3", "lapse", "shift1", "shift2", "shift3", "b1", "b2", "b3", "A1", "A2", "A3", "B11", "B21", "B31", "B12", "B22", "B32", "B13", "B23", "B33", "D111", "D112", "D113", "D122", "D123", "D133", "D211", "D212", "D213", "D222", "D223", "D233", "D311", "D312", "D313", "D322", "D323", "D333", "KK", "phi", "P1", "P2", "P3", "K0" };

  for(int i=0; i<nVar; i++) {
    char EvolvedIndex[100], RHSIndex[100];
    sprintf(EvolvedIndex, "Okapi::%s", state_vector_names[i]);
    sprintf(RHSIndex,     "Okapi::%s_rhs", state_vector_names[i]);
    // for debugging, activate this:
    // CCTK_VInfo(CCTK_THORNSTRING, "MoL Register Evolved %s => %s", EvolvedIndex, RHSIndex);
    ierr += MoLRegisterEvolved(CCTK_VarIndex(EvolvedIndex),
                               CCTK_VarIndex(RHSIndex));
  }

  if (ierr != 0)
     CCTK_WARN(CCTK_WARN_ABORT, "Problems registering Okapi variables with MoL");

  return;
}


