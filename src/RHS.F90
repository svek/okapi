#include "cctk.h"

! u

RECURSIVE SUBROUTINE Okapi_Do_CalcRHS(u)
    USE typesDef, ONLY : nVar,  nDim, EQN, d
    IMPLICIT NONE
    ! Argument list 
    CCTK_REAL, INTENT(IN)  ::  Q(nVar), gradQ(nVar,d)  
    CCTK_REAL, INTENT(OUT) :: Src_BgradQ(nVar) 


END SUBROUTINE Okapi_Do_CalcRHS
