#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>

// This file is quite likely only bogus administrative stuff and nobody
// understands what it is good for. It basically comes so from the
// ML_CCZ4_Helper thorn.

static void select_bcs(const cGH *cctkGH, const char *gn);

void Okapi_SelectBCs(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;

  select_bcs(cctkGH, "Okapi::state_vector");
  /*
  select_bcs(cctkGH, "ADMBase::lapse");
  select_bcs(cctkGH, "ADMBase::shift");
  */
}

static void select_bcs(const cGH *cctkGH, const char *gn) {
  DECLARE_CCTK_PARAMETERS;

  // In McLachlan, this is a number which is registered via param.ccl as
  // SHARES: GenericFD
  // USES CCTK_INT boundary_width
  //  should therefore coincide with the boundary layer width.
  // However, the Kranc thorn calls this "deprecated"
  int boundary_width = 1;
  
  int ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, boundary_width,
                                       -1, gn, "none");
  
  if (ierr != 0)
     CCTK_WARN(CCTK_WARN_ABORT, "Problems selecting BC for Okapi");
}
