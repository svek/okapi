#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "cctk_Functions.h"

SUBROUTINE Okapi_to_ADM(CCTK_ARGUMENTS)
    USE typesDef, ONLY : nVar
    IMPLICIT NONE

    DECLARE_CCTK_ARGUMENTS
    DECLARE_CCTK_FUNCTIONS

    ! Local variables 
    INTEGER :: i,k,j
    CCTK_REAL :: g_cov(3,3), Aex(3,3), shift(3)
    CCTK_REAL :: lap, P ! lapse and phi, locally in Fortran (<-> Cactus)
    CCTK_REAL, PARAMETER :: Pi = ACOS(-1.0) 
    !
    
    DO i = 1, cctk_lsh(1)
    DO j = 1, cctk_lsh(2)
    DO k = 1, cctk_lsh(3)

    ! in contrast to Okapi_from_ADM, we don't go the long way via the state
    ! vector. This is because no code exists and I wrote this from scratch.

    ! Cons2Prim on Okapi Cactus variables -> Local Fortran
    P = EXP(MAX(-20.,MIN(20., phi(i,j,k))))  
    lap = EXP(MAX(-20.,MIN(20., lapse(i,j,k))))
    
    shift(1) = shift1(i,j,k)
    shift(2) = shift2(i,j,k)
    shift(3) = shift3(i,j,k)

    g_cov(1,1) = g11(i,j,k) / P**2
    g_cov(1,2) = g12(i,j,k) / P**2
    g_cov(1,3) = g13(i,j,k) / P**2
    g_cov(2,2) = g22(i,j,k) / P**2
    g_cov(2,3) = g23(i,j,k) / P**2
    g_cov(3,3) = g33(i,j,k) / P**2
    
    Aex(1,1) = A11(i,j,k) / P**2 + 1./3. * KK(i,j,k) * g_cov(1,1)
    Aex(1,2) = A12(i,j,k) / P**2 + 1./3. * KK(i,j,k) * g_cov(1,2)
    Aex(1,3) = A13(i,j,k) / P**2 + 1./3. * KK(i,j,k) * g_cov(1,3)
    Aex(2,2) = A22(i,j,k) / P**2 + 1./3. * KK(i,j,k) * g_cov(2,2)
    Aex(2,3) = A23(i,j,k) / P**2 + 1./3. * KK(i,j,k) * g_cov(2,3)
    Aex(3,3) = A33(i,j,k) / P**2 + 1./3. * KK(i,j,k) * g_cov(3,3)
    
    ! Write all local Fortran out to ADMBase:
    gxx(i,j,k) = g_cov(1,1)
    gxy(i,j,k) = g_cov(1,2)
    gxz(i,j,k) = g_cov(1,3)
    gyy(i,j,k) = g_cov(2,2)
    gyz(i,j,k) = g_cov(2,3)
    gzz(i,j,k) = g_cov(3,3)

    kxx(i,j,k) = Aex(1,1)
    kxy(i,j,k) = Aex(1,2)
    kxz(i,j,k) = Aex(1,3)
    kyy(i,j,k) = Aex(2,2)
    kyz(i,j,k) = Aex(2,3)
    kzz(i,j,k) = Aex(3,3)
    
    alp(i,j,k) = lap
    betax(i,j,k) = shift(1)
    betay(i,j,k) = shift(2)
    betaz(i,j,k) = shift(3)
    
    END DO
    END DO
    END DO
    ! end of grid loop
    
END SUBROUTINE Okapi_to_ADM

SUBROUTINE Okapi_from_ADM(CCTK_ARGUMENTS)
    USE typesDef, ONLY : nVar
    IMPLICIT NONE

    DECLARE_CCTK_ARGUMENTS
    DECLARE_CCTK_FUNCTIONS

    ! Actual Argument list 
    CCTK_REAL :: U0(nVar), V0(nVar)   ! Initial data Conserved and Primitive vector
    ! Local variables 
    INTEGER :: ind(3), ind1, ind2, ind3, i,k,j,l !n(3)
    CCTK_REAL :: g_cov(3,3), g_contr(3,3), Kex(3,3), Aex(3,3), shift(3)
    CCTK_REAL :: traceK, AA(3), BB(3,3), DD(3,3,3), PP(3), Gtilde(3)   
    CCTK_REAL :: lap, P ! lapse and phi, locally in Fortran (<-> Cactus)
    CCTK_REAL :: delta_space(3), detg
    CCTK_REAL, PARAMETER :: Pi = ACOS(-1.0) 
    !
    u0 = 0.0
    V0 = 0.0
    
    CALL CCTK_INFO("Filling Okapi data from ADMBase data.")
    delta_space = (/ CCTK_DELTA_SPACE(1), CCTK_DELTA_SPACE(2), CCTK_DELTA_SPACE(3) /)
    
    ! First loop goes over the whole grid, including ghost zones, to set all
    ! non-aux quantities.
    
    DO ind1 = 1, cctk_lsh(1) ! x
    DO ind2 = 1, cctk_lsh(2) ! y
    DO ind3 = 1, cctk_lsh(3) ! z

    ind = (/ ind1, ind2, ind3 /)

    CALL METRIC(CCTK_PASS_FTOF, ind, lap, shift, Kex, g_cov, P, detg)
    CALL MatrixInverse3x3(g_cov, g_contr, detg)
    !CALL DMETRIC(CCTK_PASS_FTOF, ind, delta_space, AA, BB, DD, PP)

    ! Trace of K 
    traceK = SUM( Kex*g_contr ) 
    ! The conformal traceless part of the extrinsic curvature 
    Aex = P**2*( Kex - 1./3*traceK*g_cov ) 

    ! The metric tensor 
    V0(1) = P**2*g_cov(1,1)    ! \gamma_11 
    V0(2) = P**2*g_cov(1,2)    ! \gamma_12
    V0(3) = P**2*g_cov(1,3)    ! \gamma_13
    V0(4) = P**2*g_cov(2,2)    ! \gamma_22
    V0(5) = P**2*g_cov(2,3)    ! \gamma_23
    V0(6) = P**2*g_cov(3,3)    ! \gamma_33 
    ! The extrinsic curvature         
    V0(7)  = Aex(1,1)  
    V0(8)  = Aex(1,2) 
    V0(9)  = Aex(1,3) 
    V0(10) = Aex(2,2) 
    V0(11) = Aex(2,3) 
    V0(12) = Aex(3,3) 
    ! The logarithm of the lapse 
    V0(17) = lap 
    ! The shift 
    V0(18) = shift(1)  
    V0(19) = shift(2) 
    V0(20) = shift(3) 
    ! trace of K 
    V0(54) = traceK 
    ! logarithm of the conformal factor phi 
    V0(55) = P  
    !
   
    CALL PDEPrim2Cons(u0,V0) 
    !PRINT *, "U0(1)=", U0(1)
    
    ! NAN check
!    DO i=1, nVar
!      IF (ISNAN(U0(i))) THEN
!         CALL ABORT
!      END IF
!    END DO
    
    !IF( ALL(ind.eq.3) ) then
    !  PRINT *, "Okapi_from_ADM at ", ind
    !  DO i=1, nVar
    !    PRINT *, "U0(",i,") = ",U0(i)
    !  END DO
    !END IF
    
    ! Scatter the variables out
    CALL SCATTER_CCZ4(CCTK_PASS_FTOF, ind, U0)

    END DO
    END DO
    END DO
    ! end of first grid loop
    
    ! Second grid loop goes only for the aux variables to determine their
    ! values, making use of the initialized field values within the grid.

    DO ind1 = cctk_nghostzones(1), cctk_lsh(1)-cctk_nghostzones(1) ! x
    DO ind2 = cctk_nghostzones(2), cctk_lsh(2)-cctk_nghostzones(2) ! y
    DO ind3 = cctk_nghostzones(3), cctk_lsh(3)-cctk_nghostzones(3) ! z

    ind = (/ ind1, ind2, ind3 /)

    ! For convenience, read the values off again from the ADMBase.
    CALL METRIC(CCTK_PASS_FTOF, ind, lap, shift, Kex, g_cov, P, detg)
    CALL MatrixInverse3x3(g_cov, g_contr, detg)
    CALL DMETRIC(CCTK_PASS_FTOF, ind, delta_space, AA, BB, DD, PP)

    !test = matmul(g_cov,g_contr)
    AA = AA/lap   ! convert pure derivatives to aux. variables 
    DD = 0.5*DD     ! convert pure derivatives to aux. variables 
    PP = PP/P     ! convert pure derivatives to aux. variables  
    ! The contracted connection coefficients 
    Gtilde = 0.0
    DO i = 1, 3
      DO j = 1, 3
         DO k = 1, 3
           DO l = 1, 3
               Gtilde(i) = Gtilde(i) + 1./P**2*( g_contr(i,j)*g_contr(k,l)*( 2*DD(l,j,k) + 2*PP(l)*g_cov(j,k) )  ) 
           ENDDO
         ENDDO 
      ENDDO
    ENDDO 

    CALL GATHER_CCZ4(CCTK_PASS_FTOF, ind, U0)
    
    ! The cleaning variables Z and Theta 
    U0(13) = 0.0          ! Theta 
    U0(14) = Gtilde(1)    ! G1 
    U0(15) = Gtilde(2)    ! G2 
    U0(16) = Gtilde(3)    ! G3  
    !
    ! Auxiliary variables
    ! 
    !! vector A_i = \partial_i \alpha / \alpha 
    !!  
    U0(24) = AA(1)     ! A1 = alpha_1/alpha  
    U0(25) = AA(2)     ! A2 = alpha_2/alpha    
    U0(26) = AA(3)     ! A3 = alpha_3/alpha   
    !
    ! Matrix B_ik = \partial_i \beta_k 
    !
    U0(27) = BB(1,1) 
    U0(28) = BB(2,1)
    U0(29) = BB(3,1)
    U0(30) = BB(1,2)
    U0(31) = BB(2,2)
    U0(32) = BB(3,2)
    U0(33) = BB(1,3)
    U0(34) = BB(2,3)
    U0(35) = BB(3,3) 
    !
    ! tensor D_ijk = 0.5 \partial i \tilde \gamma_jk   
    !
    DO j = 1, 3
     DO i = 1, 3 
      DO k = 1, 3 
        DD(k,i,j) = P**2*( DD(k,i,j) + PP(k)*g_cov(i,j) ) 
      ENDDO
     ENDDO
    ENDDO 
    !
    U0(36) = DD(1,1,1) 
    U0(37) = DD(1,1,2) 
    U0(38) = DD(1,1,3) 
    U0(39) = DD(1,2,2) 
    U0(40) = DD(1,2,3) 
    U0(41) = DD(1,3,3) 
    U0(42) = DD(2,1,1) 
    U0(43) = DD(2,1,2) 
    U0(44) = DD(2,1,3) 
    U0(45) = DD(2,2,2) 
    U0(46) = DD(2,2,3) 
    U0(47) = DD(2,3,3) 
    U0(48) = DD(3,1,1) 
    U0(49) = DD(3,1,2) 
    U0(50) = DD(3,1,3) 
    U0(51) = DD(3,2,2) 
    U0(52) = DD(3,2,3) 
    U0(53) = DD(3,3,3) 
    ! derivative of phi 
    U0(56) = PP(1) 
    U0(57) = PP(2) 
    U0(58) = PP(3)         
    !

    ! See above first loop for debugging hints.
    
    ! Scatter the variables out
    CALL SCATTER_CCZ4(CCTK_PASS_FTOF, ind, U0)

    
    END DO
    END DO
    END DO
    ! End of second loop
    
    ! What is missing now: We don't have the Gamma and auxers yet defined
    ! in the ghost zones. This has to be done by the extrapolation routines.
    
END SUBROUTINE Okapi_from_ADM
    
SUBROUTINE METRIC(CCTK_ARGUMENTS, ind3d, lap, shift, Kex, g_cov, P, detg )
  ! Purpose: Copy ADMBase fields to Fortran arrays.
  IMPLICIT NONE
  DECLARE_CCTK_ARGUMENTS
  DECLARE_CCTK_PARAMETERS
  DECLARE_CCTK_FUNCTIONS
  !
  INTEGER, INTENT(IN) :: ind3d(3)
  CCTK_REAL, INTENT(OUT) :: lap, P, shift(3), Kex(3,3), g_cov(3,3), detg
  INTEGER           :: i, j, k
  
  i = ind3d(1)
  j = ind3d(2)
  k = ind3d(3)
  
  ! ADMBase gives me the following:
  !  gxx,gxy,gxz,gyy,gyz,gzz
  !  kxx,kxy,kxz,kyy,kyz,kzz
  !  alp
  !  betax,betay,betaz

  g_cov(1,1) = gxx(i,j,k)
  g_cov(1,2) = gxy(i,j,k)
  g_cov(2,1) = gxy(i,j,k)
  g_cov(1,3) = gxz(i,j,k)
  g_cov(3,1) = gxz(i,j,k)
  g_cov(2,2) = gyy(i,j,k)
  g_cov(2,3) = gyz(i,j,k)
  g_cov(3,2) = gyz(i,j,k)
  g_cov(3,3) = gzz(i,j,k)
  !CALL  MatrixInverse3x3(g_cov,g_contr,detg)
  detg = g_cov(1,1)*g_cov(2,2)*g_cov(3,3)-g_cov(1,1)*g_cov(2,3)*g_cov(3,2)-g_cov(2,1)*g_cov(1,2)*g_cov(3,3)+g_cov(2,1)*g_cov(1,3)*g_cov(3,2)+g_cov(3,1)*g_cov(1,2)*g_cov(2,3)-g_cov(3,1)*g_cov(1,3)*g_cov(2,2)

  Kex(1,1) = kxx(i,j,k)
  Kex(1,2) = kxy(i,j,k)
  Kex(2,1) = kxy(i,j,k)
  Kex(1,3) = kxz(i,j,k)
  Kex(3,1) = kxz(i,j,k)
  Kex(2,2) = kyy(i,j,k)
  Kex(2,3) = kyz(i,j,k)
  Kex(3,2) = kyz(i,j,k)
  Kex(3,3) = kzz(i,j,k)
    
  lap = alp(i,j,k)
  shift = (/ betax(i,j,k), betay(i,j,k), betaz(i,j,k) /)
  P = detg**(-1./6.)
  !   
END SUBROUTINE

SUBROUTINE SCATTER_CCZ4(CCTK_ARGUMENTS, ind, U)
  ! Purpose: Scatter state vector to cactus grid
  USE typesDef, ONLY : nVar
  IMPLICIT NONE
  DECLARE_CCTK_ARGUMENTS
  DECLARE_CCTK_FUNCTIONS
  !
  INTEGER, INTENT(IN) :: ind(3)
  CCTK_REAL, INTENT(IN)  :: U(nVar)
  INTEGER             :: i, j, k
  
  ! Write to cactus variables
  i = ind(1)
  j = ind(2)
  k = ind(3)
  
  g11(i,j,k) = U(1)
  g12(i,j,k) = U(2)
  g13(i,j,k) = U(3)
  g22(i,j,k) = U(4)
  g23(i,j,k) = U(5)
  g33(i,j,k) = U(6)
  A11(i,j,k) = U(7)
  A12(i,j,k) = U(8)
  A13(i,j,k) = U(9)
  A22(i,j,k) = U(10)
  A23(i,j,k) = U(11)
  A33(i,j,k) = U(12)
  Theta(i,j,k) = U(13)
  G1(i,j,k) = U(14)
  G2(i,j,k) = U(15)
  G3(i,j,k) = U(16)
  lapse(i,j,k) = U(17)
  shift1(i,j,k) = U(18)
  shift2(i,j,k) = U(19)
  shift3(i,j,k) = U(20)
  b1(i,j,k) = U(21)
  b2(i,j,k) = U(22)
  b3(i,j,k) = U(23)
  A1(i,j,k) = U(24)
  A2(i,j,k) = U(25)
  A3(i,j,k) = U(26)
  B11(i,j,k) = U(27)
  B21(i,j,k) = U(28)
  B31(i,j,k) = U(29)
  B12(i,j,k) = U(30)
  B22(i,j,k) = U(31)
  B32(i,j,k) = U(32)
  B13(i,j,k) = U(33)
  B23(i,j,k) = U(34)
  B33(i,j,k) = U(35)
  D111(i,j,k) = U(36)
  D112(i,j,k) = U(37)
  D113(i,j,k) = U(38)
  D122(i,j,k) = U(39)
  D123(i,j,k) = U(40)
  D133(i,j,k) = U(41)
  D211(i,j,k) = U(42)
  D212(i,j,k) = U(43)
  D213(i,j,k) = U(44)
  D222(i,j,k) = U(45)
  D223(i,j,k) = U(46)
  D233(i,j,k) = U(47)
  D311(i,j,k) = U(48)
  D312(i,j,k) = U(49)
  D313(i,j,k) = U(50)
  D322(i,j,k) = U(51)
  D323(i,j,k) = U(52)
  D333(i,j,k) = U(53)
  KK(i,j,k) = U(54)
  phi(i,j,k) = U(55)
  P1(i,j,k) = U(56)
  P2(i,j,k) = U(57)
  P3(i,j,k) = U(58)
  K0(i,j,k) = U(59)
END SUBROUTINE SCATTER_CCZ4

SUBROUTINE GATHER_CCZ4(CCTK_ARGUMENTS, ind, U)
  ! Purpose: Gather Cactus variables to state vector
  USE typesDef, ONLY : nVar
  IMPLICIT NONE
  DECLARE_CCTK_ARGUMENTS
  DECLARE_CCTK_PARAMETERS
  DECLARE_CCTK_FUNCTIONS
  !
  INTEGER, INTENT(IN) :: ind(3)
  CCTK_REAL, INTENT(OUT)   :: U(nVar)
  INTEGER  :: i, j, k
  i = ind(1)
  j = ind(2)
  k = ind(3)
  
  ! Read from cactus variables
  
  U(1) = g11(i,j,k)
  U(2) = g12(i,j,k)
  U(3) = g13(i,j,k)
  U(4) = g22(i,j,k)
  U(5) = g23(i,j,k)
  U(6) = g33(i,j,k)
  U(7) = A11(i,j,k)
  U(8) = A12(i,j,k)
  U(9) = A13(i,j,k)
  U(10) = A22(i,j,k)
  U(11) = A23(i,j,k)
  U(12) = A33(i,j,k)
  U(13) = Theta(i,j,k)
  U(14) = G1(i,j,k)
  U(15) = G2(i,j,k)
  U(16) = G3(i,j,k)
  U(17) = lapse(i,j,k)
  U(18) = shift1(i,j,k)
  U(19) = shift2(i,j,k)
  U(20) = shift3(i,j,k)
  U(21) = b1(i,j,k)
  U(22) = b2(i,j,k)
  U(23) = b3(i,j,k)
  U(24) = A1(i,j,k)
  U(25) = A2(i,j,k)
  U(26) = A3(i,j,k)
  U(27) = B11(i,j,k)
  U(28) = B21(i,j,k)
  U(29) = B31(i,j,k)
  U(30) = B12(i,j,k)
  U(31) = B22(i,j,k)
  U(32) = B32(i,j,k)
  U(33) = B13(i,j,k)
  U(34) = B23(i,j,k)
  U(35) = B33(i,j,k)
  U(36) = D111(i,j,k)
  U(37) = D112(i,j,k)
  U(38) = D113(i,j,k)
  U(39) = D122(i,j,k)
  U(40) = D123(i,j,k)
  U(41) = D133(i,j,k)
  U(42) = D211(i,j,k)
  U(43) = D212(i,j,k)
  U(44) = D213(i,j,k)
  U(45) = D222(i,j,k)
  U(46) = D223(i,j,k)
  U(47) = D233(i,j,k)
  U(48) = D311(i,j,k)
  U(49) = D312(i,j,k)
  U(50) = D313(i,j,k)
  U(51) = D322(i,j,k)
  U(52) = D323(i,j,k)
  U(53) = D333(i,j,k)
  U(54) = KK(i,j,k)
  U(55) = phi(i,j,k)
  U(56) = P1(i,j,k)
  U(57) = P2(i,j,k)
  U(58) = P3(i,j,k)
  U(59) = K0(i,j,k)
END SUBROUTINE GATHER_CCZ4
    
SUBROUTINE DMETRIC (CCTK_ARGUMENTS, pc, delta, dalpha, BB, DD, dphi)
  IMPLICIT NONE 
  DECLARE_CCTK_ARGUMENTS
  DECLARE_CCTK_PARAMETERS
  DECLARE_CCTK_FUNCTIONS
  !
  INTEGER, INTENT(IN)  :: pc(3) ! pc: ind3d position index of central point
  CCTK_REAL, intent(IN)  :: delta(3) ! grid spacing in each direction
  CCTK_REAL, intent(OUT) :: dalpha(3), BB(3,3), DD(3,3,3), dphi(3)
  CCTK_REAL :: detg
  CCTK_REAL :: gpl1(3,3), gmi1(3,3), apl1, ami1, betapl1(3), betami1(3) 
  CCTK_REAL :: gpl2(3,3), gmi2(3,3), apl2, ami2, betapl2(3), betami2(3) 
  CCTK_REAL :: phipl1, phipl2, phimi1, phimi2, Kex(3,3)
  INTEGER :: d, pl1(3), mi1(3), pl2(3), mi2(3)
  
  !
  ! Metric derivative computed with a fourth order central finite difference 
  !
  DO d = 1, 3
    pl1 = pc
    pl1(d) = pl1(d)+1
    mi1 = pc
    mi1(d) = mi1(d)-1
    pl2 = pc
    pl2(d) = pl2(d)+2
    mi2 = pc
    mi2(d) = mi2(d)-2
    CALL METRIC ( CCTK_PASS_FTOF, pl1, apl1, betapl1, Kex, gpl1, phipl1, detg)
    CALL METRIC ( CCTK_PASS_FTOF, mi1, ami1, betami1, Kex, gmi1, phimi1, detg)
    CALL METRIC ( CCTK_PASS_FTOF, pl2, apl2, betapl2, Kex, gpl2, phipl2, detg)
    CALL METRIC ( CCTK_PASS_FTOF, mi2, ami2, betami2, Kex, gmi2, phimi2, detg)
    dalpha(d) = ( 8.0*apl1      -8.0*ami1       +ami2       -apl2       )/(12.0*delta(d))
    BB(d,:)   = ( 8.0*betapl1(:)-8.0*betami1(:) +betami2(:) -betapl2(:) )/(12.0*delta(d))
    DD(d,:,:) = ( 8.0*gpl1(:,:) -8.0*gmi1(:,:)  +gmi2(:,:)  -gpl2(:,:)  )/(12.0*delta(d))
    dphi(d)   = ( 8.0*phipl1    -8.0*phimi1     +phimi2     -phipl2     )/(12.0*delta(d))
  ENDDO
  !
END SUBROUTINE DMETRIC  

SUBROUTINE MatrixInverse3x3(M,iM,det) 
    !---------------
    ! compute the determinant det of the NxN-matrix M
    !---------------
    IMPLICIT NONE
    ! input variables 
    CCTK_REAL, INTENT(IN)   :: M(3,3), det
    ! output variables
    CCTK_REAL, INTENT(OUT)    :: iM(3,3)
    !CCTK_REAL, INTENT(OUT)    :: det
    ! output variables
    CCTK_REAL    :: Id(3,3)
    INTEGER :: i,j
    ! 
    !det = M(1,1)*M(2,2)*M(3,3)-M(1,1)*M(2,3)*M(3,2)-M(2,1)*M(1,2)*M(3,3)+M(2,1)*M(1,3)*M(3,2)+M(3,1)*M(1,2)*M(2,3)-M(3,1)*M(1,3)*M(2,2)
    IF(det*det.LT.1e-20) THEN
        print *, 'FATAL ERROR: det = 0'
        STOP
    ENDIF
    !
    iM(1,1) =M(2,2)*M(3,3)-M(2,3)*M(3,2)
    iM(1,2) =M(1,3)*M(3,2)-M(1,2)*M(3,3)
    iM(1,3) =M(1,2)*M(2,3)-M(1,3)*M(2,2)
    iM(2,1) =M(2,3)*M(3,1)-M(2,1)*M(3,3)
    iM(2,2) =M(1,1)*M(3,3)-M(1,3)*M(3,1)
    iM(2,3) =M(1,3)*M(2,1)-M(1,1)*M(2,3)
    iM(3,1) =M(2,1)*M(3,2)-M(2,2)*M(3,1)
    iM(3,2) =M(1,2)*M(3,1)-M(1,1)*M(3,2)
    iM(3,3) =M(1,1)*M(2,2)-M(1,2)*M(2,1)
    iM = iM/det
    !
    Id = MATMUL(M,iM)
    DO i=1,3
        DO j=1,3
            IF(i.eq.j) THEN
                IF((Id(i,j)-1.)**2..GT.1e-18) THEN
                    print *, 'FATAL ERROR: iM*M !=1'
                    STOP
                ENDIF
            ELSE
                IF((Id(i,j)**2).GT.1e-18) THEN
                    print *, 'FATAL ERROR: iM*M !=1'
                    STOP
                ENDIF
            ENDIF
        ENDDO
    ENDDO
    !
    CONTINUE
    !
END SUBROUTINE MatrixInverse3x3



RECURSIVE SUBROUTINE PDECons2Prim(V,Q,iErr)
    USE typesDef, ONLY : nVar, EQN 
    IMPLICIT NONE
    ! Argument list 
    CCTK_REAL, INTENT(IN)     :: Q(nVar)     ! vector of conserved quantities 
    CCTK_REAL, INTENT(OUT)    :: V(nVar)     ! primitive variables 
    INTEGER, INTENT(OUT) :: iErr        ! error flag 
    ! Local variables 
    CCTK_REAL                 :: p 
    CCTK_REAL                 :: gamma1, gam, sb, dr, eps, sb2, sx, sy, sz, e, bx, by, bz, s2, b2 
    CCTK_REAL                 :: x1, x2, x3, v2
    CCTK_REAL                 :: w, rho, vx, vy, vz, den, vb 
    LOGICAL              :: FAILED
    CCTK_REAL, PARAMETER      :: tol = 1e-8, third=1.0/3.0, p_floor = 1.0e-5, rho_floor = 1.0e-4    
    CCTK_REAL                 :: RTSAFE_C2P_RMHD1 
    CCTK_REAL                 :: lapse, shift(3), psi, gammaij(6), g_cov(3,3), g_contr(3,3), gp, gm, dd 
    CCTK_REAL                 :: Qloc(nVar), bv(3), sm_cov(3), sm(3), bv_contr(3), vf(3), vf_cov(3)   
    CCTK_REAL                 :: QGRMHD(19), VGRMHD(19)  
    !    
    iErr = 0     
    !
    V = Q
    V(17) = EXP(MAX(-20.,MIN(20.,Q(17))))  
    V(55) = EXP(MAX(-20.,MIN(20.,Q(55))))  
    !
END SUBROUTINE PDECons2Prim 
        
RECURSIVE SUBROUTINE PDEPrim2Cons(Q,V)
    USE typesDef, ONLY : nVar, d, EQN 
    IMPLICIT NONE
    ! Argument list 
    CCTK_REAL, INTENT(IN)      :: V(nVar)     ! primitive variables 
    CCTK_REAL, INTENT(OUT)     :: Q(nVar)     ! vector of conserved quantities 
    ! Local variables 
    CCTK_REAL                  :: rho, vx, vy, vz, p, bx, by, bz, ex, ey, ez, v2, b2, e2    
    CCTK_REAL                  :: lf, gamma1, w, ww, uem 
    CCTK_REAL                  :: gp, gm, g_cov(3,3), g_contr(3,3), bv(3), vf_cov(3), psi, lapse, shift(3)
    CCTK_REAL                  :: vf(3), bv_contr(3), qv_contr(3), qb_contr(3), vxb(3), vb_cov(3), b2_cov, vxb_contr(3) 
    CCTK_REAL                  :: QGRMHD(19), VGRMHD(19)
    !
    Q = V   
    Q(17) = LOG(V(17))
    Q(55) = LOG(V(55)) 
    !
END SUBROUTINE PDEPrim2Cons      
