FO-CCZ4-F for Cactus: Fortran routines
======================================

This folder is a 1:1 copy of 

   /AstroApplications/CCZ4/Fortran

in the Astrophysics repository at

   https://gitlab.lrz.de/exahype/ExaHyPE-Astrophysics

I want to keep the changes only minimal in order to allow
an easy diff and to sync PDE changes.

-- SvenK, 2018-01-25.
