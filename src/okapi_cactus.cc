#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"

#include "PDE.h"
#include "state_vector.h"
#include "finite_differences.h"

#include <fenv.h> // nan tracker
#include <cmath> // std::isnan

void Okapi_SetID_at(const cGH *cctkGH, int i, int j, int k) {
	DECLARE_CCTK_ARGUMENTS;
	DECLARE_CCTK_PARAMETERS;

	int const ijk = CCTK_GFINDEX3D(cctkGH,i,j,k);
	double pos[] = { x[ijk], y[ijk], z[ijk] };
	
	double t = cctkGH->cctk_time; // coordinate time
	double Q[nVar];
	InitialGaugeWave(Q, pos, t);
	SCATTER(Q, ijk);
	
	// set previous time levels:
	// cf. http://einsteintoolkit.org/thornguide/CactusNumerical/MoL/documentation.html
	// "Ideally, initial data thorns should always set initial data at all time levels."
	/*
	
	double dt = cctkGH->cctk_delta_time;
	InitialGaugeWave(Q, pos, t - dt);
	SCATTER_d(Q, ijk);
	InitialGaugeWave(Q, pos, t - 2*dt);
	SCATTER_d_d(Q, ijk);
	
	*/
}

/**
 * Sets initial data in the whole simlation domain
 **/
extern "C" void Okapi_SetTestID(CCTK_ARGUMENTS) {
	DECLARE_CCTK_ARGUMENTS;
	DECLARE_CCTK_PARAMETERS;
	
	// with the time information, we can comprehend if all previous timelevels are also set by Carpet
	CCTK_VInfo(CCTK_THORNSTRING, "Setting my own GaugeWave ID at t=%f", cctkGH->cctk_time);

	// #pragma omp parallel for
	for(int k = 0; k < cctk_lsh[2]; ++k)   // z
	for(int j = 0; j < cctk_lsh[1]; ++j)   // y
	for(int i = 0; i < cctk_lsh[0]; ++i) { // x
		Okapi_SetID_at(cctkGH,i,j,k);
	}
}

/**
 * Sets initial data in the boundary region.
 **/
extern "C" void Okapi_SetExactBC(CCTK_ARGUMENTS) {
	DECLARE_CCTK_ARGUMENTS;
	DECLARE_CCTK_PARAMETERS;
	
	if(verbose) CCTK_INFO("Filling Ghost layers with initial data.");
	// fill the ghost layers with the initial data.
	// For a non-MPI single carpet component run, this is equal to exact boundary conditions.
	// For an MPI run, this means effectively the ranks do no more communicate.

	// buffer:
	// number of cells beyond the ghosts to fill, in order to avoid that
	// the ghosts do strange stuff.
	const int buffer = exact_bc_buffer;
	
	for(int dir=0; dir<3; dir++) {
		// Important: If you do this, make sure the domain is large enough so you
		// don't fill up the whole domain with 
		if(cctk_lsh[dir]-cctk_nghostzones[dir]-buffer < cctk_nghostzones[dir]+buffer) {
			CCTK_ERROR("Your domain is too small for the number of buffer cells beyond the ghost zone.");
		}
	}

	#pragma omp parallel for
	for(int k = 0; k < cctk_lsh[2]; ++k)    // z
	for(int j = 0; j < cctk_lsh[1]; ++j)    // y
	for(int i = 0; i < cctk_lsh[0]; ++i) {  // x
		// Find ghost zones, this can even be larger then the ghost zones.
		if(k <= cctk_nghostzones[2]+buffer || k >= cctk_lsh[2]-cctk_nghostzones[2]-buffer ||
			j <= cctk_nghostzones[1]+buffer || j >= cctk_lsh[1]-cctk_nghostzones[1]-buffer ||
			i <= cctk_nghostzones[0]+buffer || i >= cctk_lsh[0]-cctk_nghostzones[0]-buffer) {
			Okapi_SetID_at(cctkGH,i,j,k);
		} // if inside GZ
	} // for space
}

extern "C" void Okapi_Apply_Constraints(CCTK_ARGUMENTS){
	DECLARE_CCTK_ARGUMENTS;
	DECLARE_CCTK_PARAMETERS;
	
	// Enable all floating point exceptions but FE_INEXACT
	//feenableexcept(FE_INVALID | FE_OVERFLOW);
	
	// We set the BC data here to avoid NaNs working on the data.
	// In principle, this should not be neccessary at all.
	if(exact_boundary_conditions) {
		Okapi_SetExactBC(CCTK_PASS_CTOC);
	}
	
	if(verbose) CCTK_INFO("Applying CCZ4 Constraints");
	
	#pragma omp parallel for
	for(int k = cctk_nghostzones[2]; k < cctk_lsh[2]-cctk_nghostzones[2]; ++k)   // z
	for(int j = cctk_nghostzones[1]; j < cctk_lsh[1]-cctk_nghostzones[1]; ++j)   // y
	for(int i = cctk_nghostzones[0]; i < cctk_lsh[0]-cctk_nghostzones[0]; ++i) { // x
		int const ijk = CCTK_GFINDEX3D(cctkGH,i,j,k);
		double Q[nVar];
		GATHER(Q, ijk);
		
		//for(int m=0; m<nVar; m++) printf("Okapi_Apply_Constraints pre  at i,j,k=%d,%d,%d, Q[%d]=%f\n", i,j,k,m,Q[m]);
		for(int m=0; m<nVar; m++) if(check_nans && std::isnan(Q[m])) printf("ApplyConstraints: At i,j,k=%d,%d,%d, NaN detected: Q[%d]=%f\n", i,j,k,m, Q[m]);

		// Part of the scheme is to enforce some constraints
		CCZ4Fortran::EnforceCCZ4Constraints(Q);
		
		//for(int m=0; m<nVar; m++) printf("Okapi_Apply_Constraints post at i,j,k=%d,%d,%d, Q[%d]=%f\n", i,j,k,m,Q[m]);
		SCATTER(Q, ijk);
	}
}

extern "C" void Okapi_CalcRHS(CCTK_ARGUMENTS){
	DECLARE_CCTK_ARGUMENTS;
	DECLARE_CCTK_PARAMETERS;
	
	if(exact_boundary_conditions) {
		Okapi_SetExactBC(CCTK_PASS_CTOC);
	}
	
	// Enable all floating point exceptions but FE_INEXACT
	//feenableexcept(FE_INVALID | FE_OVERFLOW);
	
	double delta[] = { CCTK_DELTA_SPACE(0), CCTK_DELTA_SPACE(1), CCTK_DELTA_SPACE(2) };
	CCTK_REAL* beta[] = { shift1, shift2, shift3 };
	
	// TODO: If you still look for sources of bugs, remove the OMP. Probably
	//       Fortran and shared memory parallelization don't go well together!!!
	
	#pragma omp parallel for
	for(int k = cctk_nghostzones[2]; k < cctk_lsh[2]-cctk_nghostzones[2]; ++k)   // z
	for(int j = cctk_nghostzones[1]; j < cctk_lsh[1]-cctk_nghostzones[1]; ++j)   // y
	for(int i = cctk_nghostzones[0]; i < cctk_lsh[0]-cctk_nghostzones[0]; ++i) { // x
		int const ijk = CCTK_GFINDEX3D(cctkGH,i,j,k);
		double Q[nVar], gradQ[nDim][nVar], Upwinding_gradQ[nDim][nVar], fusedSource[nVar];

		// compose state vector Q
		GATHER(Q, ijk);

		// Ensure we don't have NaNs.
		//for(int m=0; m<nVar; m++) printf("Okapi_CalcRHS at i,j,k=%d,%d,%d, Q[%d]=%f\n", i,j,k,m,Q[m]);
		for(int m=0; m<nVar; m++) if(check_nans && std::isnan(Q[m])) printf("CalcRHS1 At i,j,k=%d,%d,%d, NaN detected: Q[%d]=%f\n", i,j,k,m, Q[m]);

		// compute gradQ(nVar,nDim) (C: gradQ[nDim][nVar])

		if(test_zero_pde) {
			// \partial_t Q = 0 gets no gradients for speed.
			CCZ4Fortran::PDE::fusedSourceQt(Q, gradQ[0], fusedSource);
		} else if(test_toy_pde) {
			// Toy PDE gets usual gradients
			Okapi_central_finite_differences(cctkGH,i,j,k,gradQ);
			CCZ4Fortran::PDE::fusedSourceToy(Q, gradQ[0], fusedSource);
		} else if(upwinding) {
			// This PDE should be used in real physics production runs since
			// it uses advected lie derivatives
			Okapi_central_finite_differences(cctkGH,i,j,k,gradQ);
			Okapi_advective_finite_differences(cctkGH, beta, i, j, k, Upwinding_gradQ);
			CCZ4Fortran::PDE::fusedSource(Q, gradQ[0], fusedSource);
		} else {
			// Dumbsers PDE which does not know about upwinding of beta^i partial_i
			// since it is not neccessary in the presence of a Riemann Solver
			// (as in his codes).
			Okapi_central_finite_differences(cctkGH,i,j,k,gradQ);
			CCZ4Fortran::PDE::fusedSource(Q, gradQ[0], fusedSource);
		}

		//for(int m=0; m<nVar; m++) printf("Okapi_CalcRHS at i,j,k=%d,%d,%d, Q[%d]=%f\n", i,j,k,m,Q[m]);
		for(int m=0; m<nVar; m++) if(check_nans && std::isnan(Q[m])) printf("CaclRHS2 At i,j,k=%d,%d,%d, NaN detected: Q[%d]=%f\n", i,j,k,m, Q[m]);

		// Similarly to Antelope, we compute the constraint violations at this place.
		// For us it is only convenient because we already have the derivatives.
		// However, it should happen after the timestep actually.
		double constraints[6];
		CCZ4Fortran::DeriveADMConstraints(constraints, Q, gradQ[0]);
		// Scatter constraints:
		H[ijk] = constraints[0];
		M1[ijk] = constraints[1];
		M2[ijk] = constraints[2];
		M3[ijk] = constraints[3];
		detg_minus1[ijk] = constraints[4];
		traceA[ijk] = constraints[5];

		SCATTER_RHS(fusedSource, ijk);
	} // for space
} // end Okapi_CalcRHS

extern "C" void Okapi_AddDissipation(CCTK_ARGUMENTS){
	DECLARE_CCTK_ARGUMENTS;
	DECLARE_CCTK_PARAMETERS;
	
	double delta[] = { CCTK_DELTA_SPACE(0), CCTK_DELTA_SPACE(1), CCTK_DELTA_SPACE(2) };

	// See https://gitlab.lrz.de/exahype/ExaHyPE-Engine/issues/224 for a summary of this method.
		
	// Summarizing, use this dissipation operator instead of what Cactus provides. It should
	// go well with RK4.
		
	if(verbose) CCTK_INFO("Adding dissipation operator");
	
	// just to be 100% sure...
	if(exact_boundary_conditions) {
		Okapi_SetExactBC(CCTK_PASS_CTOC);
	}

	ALIAS_STATE(Q);
	ALIAS_RHS(dudt);

	#define   u(n,i,j,k)  Q[n][CCTK_GFINDEX3D(cctkGH,i,j,k)]

	// maximum eigenvalue = speed of light, for simplicity.
	// const double smax = 1.0;
	// Dumbser likes to clean faster then speed of light.
	const double smax = e; // PDE parameter

	#pragma omp parallel for
	for(int n=0; n<nVar; n++) // numberOfVariables in the state vector
	for(int k = cctk_nghostzones[2]; k < cctk_lsh[2]-cctk_nghostzones[2]; ++k)   // z
	for(int j = cctk_nghostzones[1]; j < cctk_lsh[1]-cctk_nghostzones[1]; ++j)   // y
	for(int i = cctk_nghostzones[0]; i < cctk_lsh[0]-cctk_nghostzones[0]; ++i) {   // x
		int const ijk = CCTK_GFINDEX3D(cctkGH,i,j,k);
		dudt[n][ijk] -= 1.0/delta[0]* 3.0/256.0*smax* ( -15.0*u(n,i+1,j,k)-u(n,i+3,j,k)-15.0*u(n,i-1,j,k)+6.0*u(n,i+2,j,k)+20.0*u(n,i,j,k)+6.0*u(n,i-2,j,k)-u(n,i-3,j,k) );
		dudt[n][ijk] -= 1.0/delta[1]* 3.0/256.0*smax* ( -15.0*u(n,i,j+1,k)-u(n,i,j+3,k)-15.0*u(n,i,j-1,k)+6.0*u(n,i,j+2,k)+20.0*u(n,i,j,k)+6.0*u(n,i,j-2,k)-u(n,i,j-3,k) ); 
		dudt[n][ijk] -= 1.0/delta[2]* 3.0/256.0*smax* ( -15.0*u(n,i,j,k+1)-u(n,i,j,k+3)-15.0*u(n,i,j,k-1)+6.0*u(n,i,j,k+2)+20.0*u(n,i,j,k)+6.0*u(n,i,j,k-2)-u(n,i,j,k-3) ); 
	} // for space + variables
} // Okapi_AddDissipation

