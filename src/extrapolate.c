#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>

#include <stdbool.h>

#include "state_vector.h"

static void extrap(const cGH *cctkGH, CCTK_REAL *var) {
  ExtrapolateGammas(cctkGH, var);
}

/**
 * Extrapolate fields into ghost zones. This is only neccessary for the outer boundary
 * conditions at t=0 and happens as a "reparation step" after the Okapi_from_ADM
 * function. The idea is the following:
 * 
 *   1. In Okapi_from_ADM, compute all SO-CCZ4 quantities on the whole domain
 *      including ghost zones.
 *   2. In Okapi_from_ADM, compute then all FO-CCZ4 auxilliary quantities
 *         defined like A_i = partial_i alpha , ...
 *      on the domain but excluding the ghost zones.
 *   3. In Okapi_ExtrapolateTeq0, extrapolate the auxilliary quantities to
 *      the ghost zones. This procedure is similar as if we would have
 *      computed the auxilliary quantities with one-sided stencils.
 **/
void Okapi_ExtrapolateTeq0(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  CCTK_INFO("Extrapolating Okapi quantities");
  
  CCTK_REAL* extrap_fields[] = {
    // The Gammas are defined via auxers:
    G1, G2, G3,
    
    // aux lapse and aux phi
    A1, A2, A3,                    
    P1, P2, P3,                        
    
    // aux shift
    B11,                           
    B21,                           
    B31,                           
    B12,                           
    B22,                           
    B32,                           
    B13,                           
    B23,                           
    B33,                           
    
    // aux metric
    D111,                          
    D112,                          
    D113,                          
    D122,                          
    D123,                          
    D133,                          
    D211,                          
    D212,                          
    D213,                          
    D222,                          
    D223,                          
    D233,                          
    D311,                          
    D312,                          
    D313,                          
    D322,                          
    D323,                          
    D333
  };

  for(int i=0; i<sizeof(extrap_fields)/sizeof(extrap_fields[0]); i++)
    extrap(cctkGH, extrap_fields[i]);
}
