#ifndef __OKAPI_STATE_VECTOR__
#define __OKAPI_STATE_VECTOR__

/**
 * Translating between Cactus and state vector language.
 * Making Finite differencing.
 **/

// call with: foo(CCTK_PASS_CTOC)
// have then void foo(const cGH *cctkGH)
//  and then  DECLARE_CCTK_ARGUMENTS;


/// length of state vector
#define nVar 59

/// Number of dimensions
#define nDim 3

/**
 * Gather: Collect the Cactus spacetime arrays into a single
 *         state vector which is the same as in ExaHyPE.
 **/
#define GATHER(Q, ijk) \
	Q[ 0] = g11[ijk]    ;\
	Q[ 1] = g12[ijk]    ;\
	Q[ 2] = g13[ijk]    ;\
	Q[ 3] = g22[ijk]    ;\
	Q[ 4] = g23[ijk]    ;\
	Q[ 5] = g33[ijk]    ;\
	Q[ 6] = A11[ijk]    ;\
	Q[ 7] = A12[ijk]    ;\
	Q[ 8] = A13[ijk]    ;\
	Q[ 9] = A22[ijk]    ;\
	Q[10] = A23[ijk]    ;\
	Q[11] = A33[ijk]    ;\
	Q[12] = Theta[ijk]  ;\
	Q[13] = G1[ijk]     ;\
	Q[14] = G2[ijk]     ;\
	Q[15] = G3[ijk]     ;\
	Q[16] = lapse[ijk]  ;\
	Q[17] = shift1[ijk] ;\
	Q[18] = shift2[ijk] ;\
	Q[19] = shift3[ijk] ;\
	Q[20] = b1[ijk]     ;\
	Q[21] = b2[ijk]     ;\
	Q[22] = b3[ijk]     ;\
	Q[23] = A1[ijk]     ;\
	Q[24] = A2[ijk]     ;\
	Q[25] = A3[ijk]     ;\
	Q[26] = B11[ijk]    ;\
	Q[27] = B21[ijk]    ;\
	Q[28] = B31[ijk]    ;\
	Q[29] = B12[ijk]    ;\
	Q[30] = B22[ijk]    ;\
	Q[31] = B32[ijk]    ;\
	Q[32] = B13[ijk]    ;\
	Q[33] = B23[ijk]    ;\
	Q[34] = B33[ijk]    ;\
	Q[35] = D111[ijk]   ;\
	Q[36] = D112[ijk]   ;\
	Q[37] = D113[ijk]   ;\
	Q[38] = D122[ijk]   ;\
	Q[39] = D123[ijk]   ;\
	Q[40] = D133[ijk]   ;\
	Q[41] = D211[ijk]   ;\
	Q[42] = D212[ijk]   ;\
	Q[43] = D213[ijk]   ;\
	Q[44] = D222[ijk]   ;\
	Q[45] = D223[ijk]   ;\
	Q[46] = D233[ijk]   ;\
	Q[47] = D311[ijk]   ;\
	Q[48] = D312[ijk]   ;\
	Q[49] = D313[ijk]   ;\
	Q[50] = D322[ijk]   ;\
	Q[51] = D323[ijk]   ;\
	Q[52] = D333[ijk]   ;\
	Q[53] = KK[ijk]     ;\
	Q[54] = phi[ijk]    ;\
	Q[55] = P1[ijk]     ;\
	Q[56] = P2[ijk]     ;\
	Q[57] = P3[ijk]     ;\
	Q[58] = K0[ijk]     ;

/**
 * Scatter: Copy the state vector to Cactus spacetime arrays.
 **/
#define SCATTER_RHS(Q, ijk) \
	g11_rhs[ijk]    = Q[ 0];\
	g12_rhs[ijk]    = Q[ 1];\
	g13_rhs[ijk]    = Q[ 2];\
	g22_rhs[ijk]    = Q[ 3];\
	g23_rhs[ijk]    = Q[ 4];\
	g33_rhs[ijk]    = Q[ 5];\
	A11_rhs[ijk]    = Q[ 6];\
	A12_rhs[ijk]    = Q[ 7];\
	A13_rhs[ijk]    = Q[ 8];\
	A22_rhs[ijk]    = Q[ 9];\
	A23_rhs[ijk]    = Q[10];\
	A33_rhs[ijk]    = Q[11];\
	Theta_rhs[ijk]  = Q[12];\
	G1_rhs[ijk]     = Q[13];\
	G2_rhs[ijk]     = Q[14];\
	G3_rhs[ijk]     = Q[15];\
	lapse_rhs[ijk]  = Q[16];\
	shift1_rhs[ijk] = Q[17];\
	shift2_rhs[ijk] = Q[18];\
	shift3_rhs[ijk] = Q[19];\
	b1_rhs[ijk]     = Q[20];\
	b2_rhs[ijk]     = Q[21];\
	b3_rhs[ijk]     = Q[22];\
	A1_rhs[ijk]     = Q[23];\
	A2_rhs[ijk]     = Q[24];\
	A3_rhs[ijk]     = Q[25];\
	B11_rhs[ijk]    = Q[26];\
	B21_rhs[ijk]    = Q[27];\
	B31_rhs[ijk]    = Q[28];\
	B12_rhs[ijk]    = Q[29];\
	B22_rhs[ijk]    = Q[30];\
	B32_rhs[ijk]    = Q[31];\
	B13_rhs[ijk]    = Q[32];\
	B23_rhs[ijk]    = Q[33];\
	B33_rhs[ijk]    = Q[34];\
	D111_rhs[ijk]   = Q[35];\
	D112_rhs[ijk]   = Q[36];\
	D113_rhs[ijk]   = Q[37];\
	D122_rhs[ijk]   = Q[38];\
	D123_rhs[ijk]   = Q[39];\
	D133_rhs[ijk]   = Q[40];\
	D211_rhs[ijk]   = Q[41];\
	D212_rhs[ijk]   = Q[42];\
	D213_rhs[ijk]   = Q[43];\
	D222_rhs[ijk]   = Q[44];\
	D223_rhs[ijk]   = Q[45];\
	D233_rhs[ijk]   = Q[46];\
	D311_rhs[ijk]   = Q[47];\
	D312_rhs[ijk]   = Q[48];\
	D313_rhs[ijk]   = Q[49];\
	D322_rhs[ijk]   = Q[50];\
	D323_rhs[ijk]   = Q[51];\
	D333_rhs[ijk]   = Q[52];\
	KK_rhs[ijk]     = Q[53];\
	phi_rhs[ijk]    = Q[54];\
	P1_rhs[ijk]     = Q[55];\
	P2_rhs[ijk]     = Q[56];\
	P3_rhs[ijk]     = Q[57];\
	K0_rhs[ijk]     = Q[58];
	

#define SCATTER(Q, ijk) \
	g11[ijk]    = Q[ 0];\
	g12[ijk]    = Q[ 1];\
	g13[ijk]    = Q[ 2];\
	g22[ijk]    = Q[ 3];\
	g23[ijk]    = Q[ 4];\
	g33[ijk]    = Q[ 5];\
	A11[ijk]    = Q[ 6];\
	A12[ijk]    = Q[ 7];\
	A13[ijk]    = Q[ 8];\
	A22[ijk]    = Q[ 9];\
	A23[ijk]    = Q[10];\
	A33[ijk]    = Q[11];\
	Theta[ijk]  = Q[12];\
	G1[ijk]     = Q[13];\
	G2[ijk]     = Q[14];\
	G3[ijk]     = Q[15];\
	lapse[ijk]  = Q[16];\
	shift1[ijk] = Q[17];\
	shift2[ijk] = Q[18];\
	shift3[ijk] = Q[19];\
	b1[ijk]     = Q[20];\
	b2[ijk]     = Q[21];\
	b3[ijk]     = Q[22];\
	A1[ijk]     = Q[23];\
	A2[ijk]     = Q[24];\
	A3[ijk]     = Q[25];\
	B11[ijk]    = Q[26];\
	B21[ijk]    = Q[27];\
	B31[ijk]    = Q[28];\
	B12[ijk]    = Q[29];\
	B22[ijk]    = Q[30];\
	B32[ijk]    = Q[31];\
	B13[ijk]    = Q[32];\
	B23[ijk]    = Q[33];\
	B33[ijk]    = Q[34];\
	D111[ijk]   = Q[35];\
	D112[ijk]   = Q[36];\
	D113[ijk]   = Q[37];\
	D122[ijk]   = Q[38];\
	D123[ijk]   = Q[39];\
	D133[ijk]   = Q[40];\
	D211[ijk]   = Q[41];\
	D212[ijk]   = Q[42];\
	D213[ijk]   = Q[43];\
	D222[ijk]   = Q[44];\
	D223[ijk]   = Q[45];\
	D233[ijk]   = Q[46];\
	D311[ijk]   = Q[47];\
	D312[ijk]   = Q[48];\
	D313[ijk]   = Q[49];\
	D322[ijk]   = Q[50];\
	D323[ijk]   = Q[51];\
	D333[ijk]   = Q[52];\
	KK[ijk]     = Q[53];\
	phi[ijk]    = Q[54];\
	P1[ijk]     = Q[55];\
	P2[ijk]     = Q[56];\
	P3[ijk]     = Q[57];\
	K0[ijk]     = Q[58];

// Scatter to previous timestep state
#define SCATTER_p(Q, ijk) \
	g11_p[ijk]    = Q[ 0];\
	g12_p[ijk]    = Q[ 1];\
	g13_p[ijk]    = Q[ 2];\
	g22_p[ijk]    = Q[ 3];\
	g23_p[ijk]    = Q[ 4];\
	g33_p[ijk]    = Q[ 5];\
	A11_p[ijk]    = Q[ 6];\
	A12_p[ijk]    = Q[ 7];\
	A13_p[ijk]    = Q[ 8];\
	A22_p[ijk]    = Q[ 9];\
	A23_p[ijk]    = Q[10];\
	A33_p[ijk]    = Q[11];\
	Theta_p[ijk]  = Q[12];\
	G1_p[ijk]     = Q[13];\
	G2_p[ijk]     = Q[14];\
	G3_p[ijk]     = Q[15];\
	lapse_p[ijk]  = Q[16];\
	shift1_p[ijk] = Q[17];\
	shift2_p[ijk] = Q[18];\
	shift3_p[ijk] = Q[19];\
	b1_p[ijk]     = Q[20];\
	b2_p[ijk]     = Q[21];\
	b3_p[ijk]     = Q[22];\
	A1_p[ijk]     = Q[23];\
	A2_p[ijk]     = Q[24];\
	A3_p[ijk]     = Q[25];\
	B11_p[ijk]    = Q[26];\
	B21_p[ijk]    = Q[27];\
	B31_p[ijk]    = Q[28];\
	B12_p[ijk]    = Q[29];\
	B22_p[ijk]    = Q[30];\
	B32_p[ijk]    = Q[31];\
	B13_p[ijk]    = Q[32];\
	B23_p[ijk]    = Q[33];\
	B33_p[ijk]    = Q[34];\
	D111_p[ijk]   = Q[35];\
	D112_p[ijk]   = Q[36];\
	D113_p[ijk]   = Q[37];\
	D122_p[ijk]   = Q[38];\
	D123_p[ijk]   = Q[39];\
	D133_p[ijk]   = Q[40];\
	D211_p[ijk]   = Q[41];\
	D212_p[ijk]   = Q[42];\
	D213_p[ijk]   = Q[43];\
	D222_p[ijk]   = Q[44];\
	D223_p[ijk]   = Q[45];\
	D233_p[ijk]   = Q[46];\
	D311_p[ijk]   = Q[47];\
	D312_p[ijk]   = Q[48];\
	D313_p[ijk]   = Q[49];\
	D322_p[ijk]   = Q[50];\
	D323_p[ijk]   = Q[51];\
	D333_p[ijk]   = Q[52];\
	KK_p[ijk]     = Q[53];\
	phi_p[ijk]    = Q[54];\
	P1_p[ijk]     = Q[55];\
	P2_p[ijk]     = Q[56];\
	P3_p[ijk]     = Q[57];\
	K0_p[ijk]     = Q[58];
	
// Scatter to previous previous timestep state
#define SCATTER_p_p(Q, ijk) \
	g11_p_p[ijk]    = Q[ 0];\
	g12_p_p[ijk]    = Q[ 1];\
	g13_p_p[ijk]    = Q[ 2];\
	g22_p_p[ijk]    = Q[ 3];\
	g23_p_p[ijk]    = Q[ 4];\
	g33_p_p[ijk]    = Q[ 5];\
	A11_p_p[ijk]    = Q[ 6];\
	A12_p_p[ijk]    = Q[ 7];\
	A13_p_p[ijk]    = Q[ 8];\
	A22_p_p[ijk]    = Q[ 9];\
	A23_p_p[ijk]    = Q[10];\
	A33_p_p[ijk]    = Q[11];\
	Theta_p_p[ijk]  = Q[12];\
	G1_p_p[ijk]     = Q[13];\
	G2_p_p[ijk]     = Q[14];\
	G3_p_p[ijk]     = Q[15];\
	lapse_p_p[ijk]  = Q[16];\
	shift1_p_p[ijk] = Q[17];\
	shift2_p_p[ijk] = Q[18];\
	shift3_p_p[ijk] = Q[19];\
	b1_p_p[ijk]     = Q[20];\
	b2_p_p[ijk]     = Q[21];\
	b3_p_p[ijk]     = Q[22];\
	A1_p_p[ijk]     = Q[23];\
	A2_p_p[ijk]     = Q[24];\
	A3_p_p[ijk]     = Q[25];\
	B11_p_p[ijk]    = Q[26];\
	B21_p_p[ijk]    = Q[27];\
	B31_p_p[ijk]    = Q[28];\
	B12_p_p[ijk]    = Q[29];\
	B22_p_p[ijk]    = Q[30];\
	B32_p_p[ijk]    = Q[31];\
	B13_p_p[ijk]    = Q[32];\
	B23_p_p[ijk]    = Q[33];\
	B33_p_p[ijk]    = Q[34];\
	D111_p_p[ijk]   = Q[35];\
	D112_p_p[ijk]   = Q[36];\
	D113_p_p[ijk]   = Q[37];\
	D122_p_p[ijk]   = Q[38];\
	D123_p_p[ijk]   = Q[39];\
	D133_p_p[ijk]   = Q[40];\
	D211_p_p[ijk]   = Q[41];\
	D212_p_p[ijk]   = Q[42];\
	D213_p_p[ijk]   = Q[43];\
	D222_p_p[ijk]   = Q[44];\
	D223_p_p[ijk]   = Q[45];\
	D233_p_p[ijk]   = Q[46];\
	D311_p_p[ijk]   = Q[47];\
	D312_p_p[ijk]   = Q[48];\
	D313_p_p[ijk]   = Q[49];\
	D322_p_p[ijk]   = Q[50];\
	D323_p_p[ijk]   = Q[51];\
	D333_p_p[ijk]   = Q[52];\
	KK_p_p[ijk]     = Q[53];\
	phi_p_p[ijk]    = Q[54];\
	P1_p_p[ijk]     = Q[55];\
	P2_p_p[ijk]     = Q[56];\
	P3_p_p[ijk]     = Q[57];\
	K0_p_p[ijk]     = Q[58];
	
	
/**
 * Alias/Shadow a state vector: Accessing the fields without copying.
 **/
#define ALIAS_STATE(all_fields) \
 CCTK_REAL* all_fields[] = {           \
	g11, g12, g13, g22, g23, g33,  \
	A11, A12, A13, A22, A23, A33,  \
	Theta, G1, G2, G3,             \
	lapse, shift1, shift2, shift3, \
	b1, b2, b3,                    \
	A1, A2, A3,                    \
	B11,                           \
	B21,                           \
	B31,                           \
	B12,                           \
	B22,                           \
	B32,                           \
	B13,                           \
	B23,                           \
	B33,                           \
	D111,                          \
	D112,                          \
	D113,                          \
	D122,                          \
	D123,                          \
	D133,                          \
	D211,                          \
	D212,                          \
	D213,                          \
	D222,                          \
	D223,                          \
	D233,                          \
	D311,                          \
	D312,                          \
	D313,                          \
	D322,                          \
	D323,                          \
	D333,                          \
	KK,                            \
	phi,                           \
	P1, P2, P3,                    \
	K0                             \
  };

/**
 * Alias only the PDE part, setting the pointers to all ODE fields to NULL.
 **/
#define ALIAS_PDE_PART(all_fields) \
 CCTK_REAL* all_fields[] = {           \
	NULL, NULL, NULL, NULL, NULL, NULL,  \
	A11, A12, A13, A22, A23, A33,  \
	Theta, G1, G2, G3,             \
	NULL, NULL, NULL, NULL, \
	b1, b2, b3,                    \
	A1, A2, A3,                    \
	B11,                           \
	B21,                           \
	B31,                           \
	B12,                           \
	B22,                           \
	B32,                           \
	B13,                           \
	B23,                           \
	B33,                           \
	D111,                          \
	D112,                          \
	D113,                          \
	D122,                          \
	D123,                          \
	D133,                          \
	D211,                          \
	D212,                          \
	D213,                          \
	D222,                          \
	D223,                          \
	D233,                          \
	D311,                          \
	D312,                          \
	D313,                          \
	D322,                          \
	D323,                          \
	D333,                          \
	KK,                            \
	phi,                           \
	NULL, NULL, NULL,              \
	K0                             \
  };
  
/**
 * Alias the RHS fields
 **/
#define ALIAS_RHS(all_fields) \
 CCTK_REAL* all_fields[] = {           \
	g11_rhs, g12_rhs, g13_rhs, g22_rhs, g23_rhs, g33_rhs,  \
	A11_rhs, A12_rhs, A13_rhs, A22_rhs, A23_rhs, A33_rhs,  \
	Theta_rhs, G1_rhs, G2_rhs, G3_rhs,             \
	lapse_rhs, shift1_rhs, shift2_rhs, shift3_rhs, \
	b1_rhs, b2_rhs, b3_rhs,                    \
	A1_rhs, A2_rhs, A3_rhs,                    \
	B11_rhs,                           \
	B21_rhs,                           \
	B31_rhs,                           \
	B12_rhs,                           \
	B22_rhs,                           \
	B32_rhs,                           \
	B13_rhs,                           \
	B23_rhs,                           \
	B33_rhs,                           \
	D111_rhs,                          \
	D112_rhs,                          \
	D113_rhs,                          \
	D122_rhs,                          \
	D123_rhs,                          \
	D133_rhs,                          \
	D211_rhs,                          \
	D212_rhs,                          \
	D213_rhs,                          \
	D222_rhs,                          \
	D223_rhs,                          \
	D233_rhs,                          \
	D311_rhs,                          \
	D312_rhs,                          \
	D313_rhs,                          \
	D322_rhs,                          \
	D323_rhs,                          \
	D333_rhs,                          \
	KK_rhs,                            \
	phi_rhs,                           \
	P1_rhs, P2_rhs, P3_rhs,                    \
	K0                             \
  };
	
#endif /* IFDEF __OKAPI_STATE_VECTOR__ */
