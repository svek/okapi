#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>

#include <math.h>
#include <stdbool.h>

#include "state_vector.h"

inline void newrad(const cGH *cctkGH, const CCTK_REAL *var, CCTK_REAL *rhs,
                   CCTK_REAL var0, CCTK_REAL v0) {
  const CCTK_INT radpower = 2;

  NewRad_Apply(cctkGH, var, rhs, var0, v0, radpower);
}

extern "C" void Okapi_NewRad(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  ALIAS_STATE(state);
  ALIAS_RHS(rhs);
  
  newrad(cctkGH, phi, phi_rhs, 0.0, 1.0); // log(phi) -> 0

  newrad(cctkGH, g11, g11_rhs, 1.0, 1.0); // sic
  newrad(cctkGH, g12, g12_rhs, 0.0, 1.0);
  newrad(cctkGH, g13, g13_rhs, 0.0, 1.0);
  newrad(cctkGH, g22, g22_rhs, 1.0, 1.0); // sic
  newrad(cctkGH, g13, g13_rhs, 0.0, 1.0);
  newrad(cctkGH, g33, g33_rhs, 1.0, 1.0); // sic

  newrad(cctkGH, G1, G1_rhs, 0.0, 1.0);
  newrad(cctkGH, G2, G2_rhs, 0.0, 1.0);
  newrad(cctkGH, G3, G3_rhs, 0.0, 1.0);

  newrad(cctkGH, Theta, Theta_rhs, 0.0, 1.0);

  newrad(cctkGH, KK, KK_rhs, 0.0, 1.0);
  newrad(cctkGH, K0, K0_rhs, 0.0, 1.0);

  newrad(cctkGH, A11, A11_rhs, 0.0, 1.0);
  newrad(cctkGH, A12, A12_rhs, 0.0, 1.0);
  newrad(cctkGH, A13, A13_rhs, 0.0, 1.0);
  newrad(cctkGH, A22, A22_rhs, 0.0, 1.0);
  newrad(cctkGH, A13, A13_rhs, 0.0, 1.0);
  newrad(cctkGH, A33, A33_rhs, 0.0, 1.0);

  newrad(cctkGH, lapse, lapse_rhs, 0.0, 1.0); // lapse->1, log(lapse) -> 0

  newrad(cctkGH, shift1, shift1_rhs, 0.0, 1.0);
  newrad(cctkGH, shift2, shift2_rhs, 0.0, 1.0);
  newrad(cctkGH, shift3, shift3_rhs, 0.0, 1.0);

  newrad(cctkGH, b1, b1_rhs, 0.0, 1.0);
  newrad(cctkGH, b2, b2_rhs, 0.0, 1.0);
  newrad(cctkGH, b3, b3_rhs, 0.0, 1.0);

  /* Reasoning for all FO-CCZ4 helpers:
   * - log(f) for f->1 at the boundary goes to 0.
   * - df/d x_i for f->0 at the boundary goes also to 0 for all i.
   */

  newrad(cctkGH, A1,   A1_rhs,   0.0, 1.0);
  newrad(cctkGH, A2,   A2_rhs,   0.0, 1.0);
  newrad(cctkGH, A3,   A3_rhs,   0.0, 1.0);
  newrad(cctkGH, B11,  B11_rhs,  0.0, 1.0);
  newrad(cctkGH, B21,  B21_rhs,  0.0, 1.0);
  newrad(cctkGH, B31,  B31_rhs,  0.0, 1.0);
  newrad(cctkGH, B12,  B12_rhs,  0.0, 1.0);
  newrad(cctkGH, B22,  B22_rhs,  0.0, 1.0);
  newrad(cctkGH, B32,  B32_rhs,  0.0, 1.0);
  newrad(cctkGH, B13,  B13_rhs,  0.0, 1.0);
  newrad(cctkGH, B23,  B23_rhs,  0.0, 1.0);
  newrad(cctkGH, B33,  B33_rhs,  0.0, 1.0);
  newrad(cctkGH, D111, D111_rhs, 0.0, 1.0);
  newrad(cctkGH, D112, D112_rhs, 0.0, 1.0);
  newrad(cctkGH, D113, D113_rhs, 0.0, 1.0);
  newrad(cctkGH, D122, D122_rhs, 0.0, 1.0);
  newrad(cctkGH, D123, D123_rhs, 0.0, 1.0);
  newrad(cctkGH, D133, D133_rhs, 0.0, 1.0);
  newrad(cctkGH, D211, D211_rhs, 0.0, 1.0);
  newrad(cctkGH, D212, D212_rhs, 0.0, 1.0);
  newrad(cctkGH, D213, D213_rhs, 0.0, 1.0);
  newrad(cctkGH, D222, D222_rhs, 0.0, 1.0);
  newrad(cctkGH, D223, D223_rhs, 0.0, 1.0);
  newrad(cctkGH, D233, D233_rhs, 0.0, 1.0);
  newrad(cctkGH, D311, D311_rhs, 0.0, 1.0);
  newrad(cctkGH, D312, D312_rhs, 0.0, 1.0);
  newrad(cctkGH, D313, D313_rhs, 0.0, 1.0);
  newrad(cctkGH, D322, D322_rhs, 0.0, 1.0);
  newrad(cctkGH, D323, D323_rhs, 0.0, 1.0);
  newrad(cctkGH, D333, D333_rhs, 0.0, 1.0);
  newrad(cctkGH, P1,   P1_rhs,   0.0, 1.0);
  newrad(cctkGH, P2,   P2_rhs,   0.0, 1.0);
  newrad(cctkGH, P3,   P3_rhs,   0.0, 1.0);
}
