#ifndef __OKAPI_FINITE_DIFF__
#define __OKAPI_FINITE_DIFF__

#include "state_vector.h" /* defines nDim, nVar */

/**
 * Standard central finite differencing at given position i,j,k. Stores result into the
 *   gradient of the state vector gradQ.
 *
 * The order is given by the thorn parameter "order".
 *
 **/
void Okapi_central_finite_differences(const cGH *cctkGH, int i, int j, int k, double gradQ[nDim][nVar]);


/**
 * Upwinding / directed finite difference stencils.
 * 
 * Parameters:
 *    beta:  The vector field which to take into account when upwinding. This will be the shift
 *           in this case. You construct this like
 *               CCTK_REAL* beta[3] = { shiftx, shifty, shiftz };
 * 
 *    i,j,k: The spatial position where to compute the gradient at.
 * 
 *    Upwind_gradQ: The upwinded gradient of the state vector (output) at the given position.
 *
 * The order shall be given by the thorn parameter "order".
 *
 **/
void Okapi_advective_finite_differences(const cGH *cctkGH, const CCTK_REAL* const beta[nDim], int i, int j, int k, double Upwind_gradQ[nDim][nVar]);

#endif /* __OKAPI_FINITE_DIFF__ */
