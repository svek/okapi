#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "Symmetry.h"

#include <stdio.h> // sprintf

#define arraylen(array) sizeof(array)/sizeof(array[0])

/**
 * Set symmetry for a given field.
 *   @arg sym: a pointer to an integer array int[3]
 *   @arg field: a string (char*)
 *   @arg suffix: a string (char*)
 * 
 * Example call, sets symmetry for Okapi::gxz
 * 
 * int sym[] = { -1, +1, -1 };
 * FSETSYM(sym, "g", "xz");
 * 
 * This is a macro due to the cctkGH.
 * Todo: Use CCTK_VWarn for proper error output
 **/
#define FSETSYM(sym,field,suffix) {\
  char name[100], name_rhs[100];\
  sprintf(name,    "Okapi::%s%s", field, suffix);\
  sprintf(name_rhs,"Okapi::%s%s_rhs", field, suffix);\
  if(SetCartSymVN(cctkGH, sym, name)) \
    CCTK_WARN(CCTK_WARN_ABORT, "Could not register field");\
  if(SetCartSymVN(cctkGH, sym, name_rhs))\
    CCTK_WARN(CCTK_WARN_ABORT, "Could not register RHS field");\
  }

/**
 * Set symmetry based on a symmetry string. Example (cf above):
 * SSETSYM("-+-", "g", "xz");
 **/
#define SSETSYM(stringy, field, suffix) {\
     int sym[3];\
     for(int i=0; i<3; i++) sym[i] = (stringy[i] == '+' ? +1 : -1);\
     FSETSYM(sym, field, suffix);\
  }

/**
 * Set symmetry based on a symmetry string. Without suffix. Example:
 * SETSYM("-+-", "gxz");
 **/
#define SETSYM(stringy, field) SSETSYM(stringy, field, "");

void Okapi_SetSymmetries(CCTK_ARGUMENTS){
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  CCTK_INFO("Setting symmetries for evolution variables.");

  /**
   * Scalars do not change when reflected at walls.
   **/
  const char *scalar[] = { "Theta", "lapse", "KK", "phi", "K0" };
  for(int v=0; v<arraylen(scalar); v++)
    SETSYM("+++", scalar[v]);
  
  /*
   * The symmetry of vectors is simple: If a vector v_i is reflected
   * on a wall orthogonal in axis d, then v_d -> -v_d.
   * This is encoded in these rules:
   */
  const char* vectors[] = { "G", "shift", "b", "A", "P" };
  for(int v=0; v<arraylen(vectors); v++) {
    SSETSYM("-++", vectors[v], "1");
    SSETSYM("+-+", vectors[v], "2");
    SSETSYM("++-", vectors[v], "3");
  }

  /**
   * Symmetric matrices g_ij ~ outer(v_i, v_j) allow to be decomposed into
   * the vector rule.
   **/
  const char* matrices[] = { "g", "A", "B" };
  for(int v=0; v<arraylen(matrices); v++) {
    SSETSYM("+++", matrices[v], "11");
    SSETSYM("--+", matrices[v], "12");
    SSETSYM("-+-", matrices[v], "13");
    SSETSYM("+++", matrices[v], "22");
    SSETSYM("+--", matrices[v], "23");
    SSETSYM("+++", matrices[v], "33");
  }

  /**
   * Symmetric tensors D_{kij} ~ v_k v_i v_j. Similar.
   **/
  SETSYM("-++", "D111"); SETSYM("+-+", "D112"); SETSYM("++-", "D113");
  SETSYM("-++", "D122"); SETSYM("+-+", "D211"); SETSYM("++-", "D223");
  SETSYM("-++", "D133"); SETSYM("+-+", "D222"); SETSYM("++-", "D311");
  SETSYM("-++", "D212"); SETSYM("+-+", "D233"); SETSYM("++-", "D322");
  SETSYM("-++", "D313"); SETSYM("+-+", "D323"); SETSYM("++-", "D333");

  SETSYM("---", "D123"); SETSYM("---", "D213"); SETSYM("---", "D312");

  return;
}